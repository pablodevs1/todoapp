FROM node:16-alpine3.14

WORKDIR /app

# ENV BASENAME=/

# COPY ./node_modules ./node_modules
COPY package.json ./

RUN npm install

COPY . .

# EXPOSE 3000

CMD [ "npm", "start" ]

# FROM node:16-alpine3.14 as builder
# RUN mkdir /ng-app
# WORKDIR /ng-app
# COPY . .
# RUN npm ci
# RUN npm run build

# FROM nginx:1.13.3-alpine
# COPY nginx/default.conf /etc/nginx/conf.d/
# RUN rm -rf /usr/share/nginx/html/*
# COPY --from=builder /ng-app/public /usr/share/nginx/html
# EXPOSE 80
# CMD ["nginx", "-g", "daemon off;"]